package rugby;

import java.util.*;

/******************************************************************************
 * This version moves from imperative style to declarative style programming.
 * The explicit loop is removed and a lamba expression is used to implement 
 * what was previously the body of a loop. A lamba expression is function
 * that can be passed as a parameter and does not have a name. Here the lambda
 * implements the Consumer interface, which takes a single parameter and 
 * produces no return value. 
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment05 {
  public static void main(String[] args) {
	// create array  
    String[] n1 = { "Northampton Saints", "Bath", "Leicester Tigers", "Saracens", "Exeter Chiefs", " Wasps", "Sale Sharks", "Harlequins", " Gloucester", "London Irish", "Newcastle Falcons", "London Welsh"};

    // convert array into list
    List<String> n2 = Arrays.asList(n1);  

    // display values using lambda expression
   //lambda expression is a function. can pass parameter.
    
    n2.forEach(name -> System.out.println(name));
    
    /*
	    * for (String n:n1) {
	        System.out.println(n)
	       }
	    */
  }
}


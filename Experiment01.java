package rugby;

/******************************************************************************
 * This program demonstrates the imperative way of storing some data and 
 * processing it.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment01 {
  public static void main(String[] args) {
	// storing of data in an array using imperative programming
    String [] n1 = {"Northampton Saints", "Bath", "Leicester Tigers", "Saracens", "Exeter Chiefs", " Wasps", "Sale Sharks", "Harlequins", " Gloucester", "London Irish", "Newcastle Falcons", "London Welsh"};

    System.out.println(n1.getClass());
    
  //print the value of this array using imperative programming
    for(int i=0;i<n1.length;i++) {
      System.out.println(n1[i]);
    }
  }
}
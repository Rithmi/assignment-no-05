package rugby;

/******************************************************************************
 * This program uses a foreach loop which is less error prone as it eliminates
 * the need for a loop control variable. 
 *  
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment02 {
  public static void main(String[] args) {
	//storing of data in an array using imperative programming  
    String[] n1 = {"Northampton Saints", "Bath", "Leicester Tigers", "Saracens", "Exeter Chiefs", " Wasps", "Sale Sharks", "Harlequins", " Gloucester", "London Irish", "Newcastle Falcons", "London Welsh" };
  //print the value of this array using for each
    for (String name : n1) {
      System.out.println(name);
    }
  }
}

/* for each
 * for (type var :array)
{
      statements using var;
}
*
*/

package rugby;

import java.util.*;
import java.util.function.Consumer;

//import Experiment06.StringPrintConsumer;


/******************************************************************************
 * This version is similar to the last but uses an anonymous inner class.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment07 {
// uses an anonymous inner class
 public void run() {
    String[] n1 = { "Northampton Saints", "Bath", "Leicester Tigers", "Saracens", "Exeter Chiefs", " Wasps", "Sale Sharks", "Harlequins", " Gloucester", "London Irish", "Newcastle Falcons", "London Welsh"};

    List<String> n2 = Arrays.asList(n1);  

    n2.forEach(new Consumer<String>() {
      public void accept(String str) {
        System.out.println(str);     
      }
    });
  }

  public static void main(String[] args) {
    new Experiment07().run();
  }
}

/*
 * class StringPrintConsumer implements Consumer<String> {
		public void accept(String str) {
			System.out.println(str);
		}
	}
	public void run() {
		String[] n1 = {"kevan", "Johm", "Matthew" };
	
		List<String> n2 = Arrays.asList(n1);
		
		n2.forEach(new StringPrintConsumer());
	}
	public static void main(String[] args) {
		new Ex6().run();
	}
}
*/
